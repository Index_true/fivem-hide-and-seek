const path = require('path');

module.exports = {
  target: 'node',
  entry: {
    
    './cl_init': [

      "./src/cl_dev.js",
      "./src/deathscreen/deathscreen.js",
      "./src/wantedlevel/wantedlevel.js",
      "./src/utils/IplLoader.js",
      "./src/blips/cl_blips.js",
      "./src/ui/notification.js",
      "./src/cl_main.js",
    ],
    './sv_init': [
      "./src/sv_dev.js",
      "./src/sv_main.js",
      "./src/sv_commands.js",
      "./src/blips/sv_blips.js"
    ]
  },
  output: {
    filename: "[name].js",
    path: "./"
  },
  
  watch: true,
  mode: 'development',
  devtool: 'inline-source-map',

}